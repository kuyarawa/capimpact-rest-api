const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/capimpact-rest-api');
const Schema = mongoose.Schema;

const companySchema = new Schema({
    company: String,
    sector: String
});

companySchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
companySchema.set('toJSON', {
    virtuals: true
});

companySchema.findById = function (cb) {
    return this.model('Companies').find({id: this.id}, cb);
};

const Company = mongoose.model('Companies', companySchema);


exports.findByEmail = (email) => {
    return Company.find({email: email});
};

exports.findById = (id) => {
    return Company.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.createCompany = (companyData) => {
    const company = new Company(companyData);
    return company.save();
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        Company.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, companies) {
                if (err) {
                    reject(err);
                } else {
                    resolve(companies);
                }
            })
    });
};

exports.patchCompany = (id, companyData) => {
    return new Promise((resolve, reject) => {
        Company.findById(id, function (err, company) {
            if (err) reject(err);
            for (let i in companyData) {
                company[i] = companyData[i];
            }
            company.save(function (err, updatedCompany) {
                if (err) return reject(err);
                resolve(updatedCompany);
            });
        });
    })

};

exports.removeById = (companyId) => {
    return new Promise((resolve, reject) => {
        Company.remove({_id: companyId}, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};

