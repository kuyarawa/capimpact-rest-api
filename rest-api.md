- payments

  ```bash
  curl --location --request GET "http://172.31.51.154:3000/payments" --header "Content-Type: application/json" --header "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YzFlOTJhODAwMjg4MTE0MDViYzEzYTEiLCJlbWFpbCI6ImpvaG5AZG9lLmNvbSIsInBlcm1pc3Npb25MZXZlbCI6NywicHJvdmlkZXIiOiJlbWFpbCIsIm5hbWUiOiJKb2huIERvZSIsInJlZnJlc2hLZXkiOiI1WVN5WmFySnNUc3g0NGRweU1POTBnPT0iLCJpYXQiOjE1NDU1MDc1NTh9.lDXsLoCr56eE6C22FSKJTLeyUnINq5ykjwK7x5Zr1y4" | jq .
  ```
