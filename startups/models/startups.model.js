const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/capimpact-rest-api');
const Schema = mongoose.Schema;

const startupSchema = new Schema({
    startup: String,
    sector: String
});

startupSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
startupSchema.set('toJSON', {
    virtuals: true
});

startupSchema.findById = function (cb) {
    return this.model('Startups').find({id: this.id}, cb);
};

const Startup = mongoose.model('Startups', startupSchema);


exports.findByEmail = (email) => {
    return Startup.find({email: email});
};

exports.findById = (id) => {
    return Startup.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.createStartup = (startupData) => {
    const startup = new Startup(startupData);
    return startup.save();
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        Startup.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, companies) {
                if (err) {
                    reject(err);
                } else {
                    resolve(companies);
                }
            })
    });
};

exports.patchStartup = (id, startupData) => {
    return new Promise((resolve, reject) => {
        Startup.findById(id, function (err, startup) {
            if (err) reject(err);
            for (let i in startupData) {
                startup[i] = startupData[i];
            }
            startup.save(function (err, updatedStartup) {
                if (err) return reject(err);
                resolve(updatedStartup);
            });
        });
    })

};

exports.removeById = (startupId) => {
    return new Promise((resolve, reject) => {
        Startup.remove({_id: startupId}, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};

