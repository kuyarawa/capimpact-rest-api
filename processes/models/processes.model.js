const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/capimpact-rest-api');
const Schema = mongoose.Schema;

const processSchema = new Schema({
    process: String
});

processSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
processSchema.set('toJSON', {
    virtuals: true
});

processSchema.findById = function (cb) {
    return this.model('Processes').find({id: this.id}, cb);
};

const Process = mongoose.model('Processes', processSchema);


exports.findById = (id) => {
    return Process.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.createProcess = (processData) => {
    const process = new Process(processData);
    return process.save();
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        Process.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, companies) {
                if (err) {
                    reject(err);
                } else {
                    resolve(companies);
                }
            })
    });
};

exports.patchProcess = (id, processData) => {
    return new Promise((resolve, reject) => {
        Process.findById(id, function (err, process) {
            if (err) reject(err);
            for (let i in processData) {
                process[i] = processData[i];
            }
            process.save(function (err, updatedProcess) {
                if (err) return reject(err);
                resolve(updatedProcess);
            });
        });
    })

};

exports.removeById = (processId) => {
    return new Promise((resolve, reject) => {
        Process.remove({_id: processId}, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};

