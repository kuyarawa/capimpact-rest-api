const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/capimpact-rest-api');
const Schema = mongoose.Schema;

const challengeSchema = new Schema({
    name: String,
    subset: Object
});

challengeSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
challengeSchema.set('toJSON', {
    virtuals: true
});

challengeSchema.findById = function (cb) {
    return this.model('Challenge').find({id: this.id}, cb);
};

const Challenge = mongoose.model('Challenge', challengeSchema);

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        Challenge.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, challenge) {
                if (err) {
                    reject(err);
                } else {
                    resolve(challenge);
                }
            })
    });
};

