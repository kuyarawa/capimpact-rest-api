define(function (require) {

    var ready = require('curl/domReady');

    return render;

    function render (entity) {
        ready(function () {
            var respElement = document.querySelector('[data-name="content"]');
            respElement.textContent += entity.content;
        });
    }

});
