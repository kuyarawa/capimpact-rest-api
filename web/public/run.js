var curl;
(function () {

    curl.config({
        main: 'cap7',
        packages: {
            // Your application's packages
            cap7: { location: 'cap7' },
            // Third-party packages
            curl: { location: 'lib/curl/src/curl' },
            rest: { location: 'lib/rest', main: 'rest' },
            when: { location: 'lib/when', main: 'when' }
        }
    });

}());
