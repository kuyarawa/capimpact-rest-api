# capimpact-rest-api

## Usage

Make sure you have mongodb installed into this host and running;
Get the project and run: `npm install`

Run `npm start`. It will initialize the server at port 3600.



- launch with `pm2`

  ```bash
  pm2 start index.js --name "capimpact-rest"
  ```

  

